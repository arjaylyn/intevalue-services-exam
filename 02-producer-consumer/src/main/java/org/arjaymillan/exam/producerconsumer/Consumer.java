package org.arjaymillan.exam.producerconsumer;

public class Consumer extends Thread {
       
    private Data data;

    public Consumer(Data data) {
        super("Data Consumer");
        this.data = data;
    }
    
    @Override
    public void run() {
        try {
            while (true) { 
                synchronized (data) 
                { 
                    // consumer thread waits while list is empty 
                    while (data.getList().isEmpty()) {                        
                        // wait for producer to add new
                        data.wait();
                    }
    
                    // getting the first entry in the list 
                    int val = data.getList().removeFirst(); 
                    System.out.println("Consumer :" + val); 
    
                    // wake up producer thread 
                    data.notify(); 

                    // and sleep 
                    Thread.sleep(1500); 
                } 
            } 
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
