package org.arjaymillan.exam.producerconsumer;

public final class App {
    public static final int PRODUCER_LIMIT = 5;

    public static void main(String[] args) {
        Data data = new Data(PRODUCER_LIMIT);

        Producer producer = new Producer(data);
        Consumer consumer = new Consumer(data);

        System.out.println("Starting..");
        producer.start();
        consumer.start();

        try {
            producer.join();
            consumer.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}
