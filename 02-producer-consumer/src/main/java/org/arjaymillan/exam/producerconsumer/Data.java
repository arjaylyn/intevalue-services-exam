package org.arjaymillan.exam.producerconsumer;

import java.util.LinkedList;

/**
 * Data that will be produced and cosumed
 */
public class Data {
    
    private LinkedList<Integer> list = new LinkedList<>();
    private int limit;

    public Data(int limit) {
        this.limit = limit;
    }

    public boolean isListReachLimit(){
        return list.size() >= limit;
    }

    public int getLimit() {
        return limit;
    }
    public void setLimit(int limit) {
        this.limit = limit;
    }

    public LinkedList<Integer> getList() {
        return list;
    }
    public void setList(LinkedList<Integer> list) {
        this.list = list;
    }
    
}
