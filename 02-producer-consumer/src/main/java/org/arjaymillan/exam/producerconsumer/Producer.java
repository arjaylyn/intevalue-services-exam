package org.arjaymillan.exam.producerconsumer;

public class Producer extends Thread {

    private Data data;

    public Producer(Data data) {
        super("Data Producer");
        this.data = data;
    }
    
    @Override
    public void run() {
        try {
            int value = 0; 
            while (true) { 
                synchronized (data) 
                { 
                    // producer thread waits while list is full 
                    while (data.isListReachLimit()) {    
                        // wait for the consumer thread to remove entries
                        data.wait();
                    }
    
                    System.out.println("Producer :" + value); 
    
                    // adds data to the list 
                    data.getList().add(value++); 
    
                    // notifies the consumer thread that 
                    // now it can start consuming 
                    data.notify();

                    // makes the working of program easier 
                    // to  understand 
                    Thread.sleep(1000); 
                } 
            } 
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
