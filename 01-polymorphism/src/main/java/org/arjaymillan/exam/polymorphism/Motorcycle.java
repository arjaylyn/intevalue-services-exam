package org.arjaymillan.exam.polymorphism;

public class Motorcycle extends Vehicle{
    
    public Motorcycle() {
        super();
    }

    public Motorcycle(String name) {
        super();
        this.setName(name);
    }

    @Override
    public double getAvgKmPerHour() {
        return 80;
    }
}
