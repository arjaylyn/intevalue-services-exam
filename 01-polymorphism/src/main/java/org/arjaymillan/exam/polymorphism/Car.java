package org.arjaymillan.exam.polymorphism;

public class Car extends Vehicle{
    
    public Car() {
        this.setWheelCount(4);
        this.setPassengerCount(4);
    }

    public Car(String name) {
        this();
        this.setName(name);
    }

    @Override
    public double getAvgKmPerHour() {
        return 100;
    }
}
