package org.arjaymillan.exam.polymorphism;

public class Vehicle {
    
    private String name;
    private int wheelCount;
    private int passengerCount;

    public Vehicle () {
        this.name = "";
        this.wheelCount = 2;
        this.passengerCount = 1;
    }
    public Vehicle (String name) {
        this();
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getWheelCount() {
        return this.wheelCount;
    }

    public void setWheelCount(int wheelCount) {
        this.wheelCount = wheelCount;
    }

    public int getPassengerCount() {
        return this.passengerCount;
    }
    
    public void setPassengerCount(int passengerCount) {
        this.passengerCount = passengerCount;
    }

    public double getAvgKmPerHour() {
        return 40;
    }
}
