package org.arjaymillan.exam.polymorphism;

/**
 * Main class for Intevalue Services Inc. exam
 * @author Arjay Millan
 */
public final class App {
    private App() {
    }

    /**
     * (1).Please implement a program using Java's polymorphic features (score 5).
     * @param args Not in use
     */
    public static void main(String[] args) {
        System.out.println("-Polymorphism-----------------------------");
        
        Vehicle car = new Car("Yaris");
        System.out.println("");
        System.out.println("Name : " + car.getName());
        System.out.println("Wheel Count : " + car.getWheelCount());
        System.out.println("Passenger Count : " + car.getPassengerCount());
        System.out.println("Avg Km per Hour : " + car.getAvgKmPerHour());

        Vehicle mc = new Motorcycle("Fury");
        System.out.println("");
        System.out.println("Name : " + mc.getName());
        System.out.println("Wheel Count : " + mc.getWheelCount());
        System.out.println("Passenger Count : " + mc.getPassengerCount());
        System.out.println("Avg Km per Hour : " + mc.getAvgKmPerHour());
    }
}
