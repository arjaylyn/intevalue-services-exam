package org.arjaymillan.exam.polymorphism;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Unit test for simple App.
 */
class AppTest {

    @Test
    void testCar() {
        Vehicle car = new Car();
        assertEquals(car.getAvgKmPerHour(), 100);
        assertEquals(car.getPassengerCount(), 4);
        assertEquals(car.getWheelCount(), 4);
    }
    
    @Test
    void testMotorcycle() {        
        Vehicle mc = new Motorcycle();
        assertEquals(mc.getAvgKmPerHour(), 80);
        assertEquals(mc.getPassengerCount(), 1);
        assertEquals(mc.getWheelCount(), 2);
    }
}
