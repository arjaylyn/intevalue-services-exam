# Getting Started

### Configure
- Update the property file for connection details and cron schedule
- Schema will automaticaly be created

### Build and Run
> mvn clean install

> java -jar ./target/forex-0.0.1-SNAPSHOT.jar

### Manual triger and get sample data
GET http://localhost:8080/api/forex

### Forex Source
GET https://www.bsp.gov.ph/_api/web/lists/getByTitle('Exchange Rate')/items?$select=Title,Unit,Symbol,PHPequivalent,EURequivalent,USDequivalent&$filter=Group eq '1'&$orderby=Ordering asc

accept: application/json

