package com.arjaymillan.exam.forex.repo;

import com.arjaymillan.exam.forex.model.Forex;

import org.springframework.data.repository.CrudRepository;

public interface ForexRepository extends CrudRepository<Forex, Long> {
    
}