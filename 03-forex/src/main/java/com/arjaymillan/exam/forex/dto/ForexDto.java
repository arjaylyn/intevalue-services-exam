package com.arjaymillan.exam.forex.dto;

import com.fasterxml.jackson.annotation.JsonAlias;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ForexDto {
    
    @JsonAlias(value = "Title")
    private String title;
    
    @JsonAlias(value = "Unit")
    private String unit;
    
    @JsonAlias(value = "Symbol")
    private String symbol;
    
    @JsonAlias(value = "PHPequivalent")
    private String phpEquivalent;

    @JsonAlias(value = "USDequivalent")
    private String usdEquivalent;
    
    @JsonAlias(value = "EURequivalent")
    private String eurEquivalent;
}
