package com.arjaymillan.exam.forex.dto;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BpiForexDto {
    
    private List<ForexDto> value;
}
