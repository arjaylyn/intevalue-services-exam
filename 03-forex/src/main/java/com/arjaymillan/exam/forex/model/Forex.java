package com.arjaymillan.exam.forex.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.springframework.lang.NonNull;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class Forex {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private @NonNull String id;
    
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "currency_code", referencedColumnName = "code")
    private Currency currency;
    
    @Column(name = "php_equivalent")
    private BigDecimal phpEquivalent;

    @Column(name = "usd_equivalent")
    private BigDecimal usdEquivalent;
    
    @Column(name = "eur_equivalent")
    private BigDecimal eurEquivalent;
}
