package com.arjaymillan.exam.forex.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import org.springframework.lang.NonNull;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter @Setter
public class Currency {
    
    @Id
    private @NonNull String code;

    @Column(nullable = false)
    private String unit;

    @Column(nullable = false)
    private String title;
}
