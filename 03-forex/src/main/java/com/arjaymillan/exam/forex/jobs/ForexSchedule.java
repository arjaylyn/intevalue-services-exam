package com.arjaymillan.exam.forex.jobs;

import java.util.Date;

import com.arjaymillan.exam.forex.dto.BpiForexDto;
import com.arjaymillan.exam.forex.service.ForexService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class ForexSchedule {
      
    @Autowired
    private ForexService forexService;
      
    @Scheduled(cron = "${forex.fetch.schedule}")
    public void runForexFetch(){
        log.info(String.format("runForexFetch at : %tc", new Date()));
        BpiForexDto bpi = forexService.getBPIForex();
        forexService.saveForex(bpi.getValue());
    }
}
