package com.arjaymillan.exam.forex.service;

import java.util.Map;
import java.util.Optional;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;

import com.arjaymillan.exam.forex.dto.BpiForexDto;
import com.arjaymillan.exam.forex.dto.ForexDto;
import com.arjaymillan.exam.forex.model.Currency;
import com.arjaymillan.exam.forex.model.Forex;
import com.arjaymillan.exam.forex.repo.CurrencyRepository;
import com.arjaymillan.exam.forex.repo.ForexRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class ForexService {
    private final String BPI_URI = "https://www.bsp.gov.ph/_api/web/lists/getByTitle('Exchange Rate')/items?$select={select}&$filter={filter}&$orderby={orderby}";
    private final String SELECT = "Title,Unit,Symbol,PHPequivalent,EURequivalent,USDequivalent";
    private final String FILTER = "Group eq '1'";
    private final String ORDER = "Ordering asc";

    @Autowired
    private CurrencyRepository currRepo;

    @Autowired
    private ForexRepository forexRepo;

    public BpiForexDto getBPIForex() {
        Map<String, String> param = new HashMap<>();
        param.put("select", SELECT);
        param.put("filter", FILTER);
        param.put("orderby", ORDER);

        return restTemplate().getForObject(BPI_URI, BpiForexDto.class, param);
    }

    public void saveForex(List<ForexDto> forexDtos){
        for (ForexDto forexDto : forexDtos) {
            Optional<Currency> currOpt = currRepo.findById(forexDto.getSymbol());
            Currency curr = null;
            if (currOpt.isEmpty()) {
                curr = new Currency();
                curr.setCode(forexDto.getSymbol());
                curr.setUnit(forexDto.getUnit());
                curr.setTitle(forexDto.getTitle());
                curr = currRepo.save(curr);
            }
            else {
                curr = currOpt.get();
            }

            Forex forex = new Forex();
            forex.setCurrency(curr);
            if(!forexDto.getEurEquivalent().equals("N/A"))
                forex.setEurEquivalent(new BigDecimal(forexDto.getEurEquivalent()));
            if(!forexDto.getEurEquivalent().equals("N/A"))
                forex.setUsdEquivalent(new BigDecimal(forexDto.getUsdEquivalent()));
            if(!forexDto.getEurEquivalent().equals("N/A"))
                forex.setPhpEquivalent(new BigDecimal(forexDto.getPhpEquivalent()));

            if(forex.getEurEquivalent() != null && forex.getUsdEquivalent() != null && forex.getPhpEquivalent() != null){
                forexRepo.save(forex);
            }
        }
    }
    
    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }
}
