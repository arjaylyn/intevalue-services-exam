package com.arjaymillan.exam.forex.repo;

import com.arjaymillan.exam.forex.model.Currency;

import org.springframework.data.repository.CrudRepository;

public interface CurrencyRepository extends CrudRepository<Currency, String> { 
    
}
