package com.arjaymillan.exam.forex.controller;

import java.util.Date;

import com.arjaymillan.exam.forex.dto.BpiForexDto;
import com.arjaymillan.exam.forex.service.ForexService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/api/forex")
public class ForexController {
    
    @Autowired
    private ForexService forexService;

	@GetMapping
    public BpiForexDto getForex(){
        log.info(String.format("getForex at : %tc", new Date()));
        BpiForexDto bpi = forexService.getBPIForex();
        forexService.saveForex(bpi.getValue());
        return bpi;
    }
    
}
