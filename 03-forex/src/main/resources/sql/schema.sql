CREATE TABLE IF NOT EXISTS currency (
	code VARCHAR(3) PRIMARY KEY,
	title VARCHAR(100) NOT NULL,
	unit VARCHAR(30) NOT NULL
);

CREATE TABLE IF NOT EXISTS forex (
	id BIGINT AUTO_INCREMENT PRIMARY KEY,
	currency_code VARCHAR(3) NOT NULL,
	php_equivalent DECIMAL(19,6),
	usd_equivalent DECIMAL(19,6),
	eur_equivalent DECIMAL(19,6),
    create_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (currency_code) 
        REFERENCES currency (code)
        ON UPDATE RESTRICT
);