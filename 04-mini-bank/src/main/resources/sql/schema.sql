CREATE TABLE IF NOT EXISTS user (
	id BIGINT AUTO_INCREMENT PRIMARY KEY,
	username VARCHAR(20) NOT NULL,
	password VARCHAR(100) NOT NULL,
    create_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    update_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    UNIQUE KEY unique_username (username)
);

CREATE TABLE IF NOT EXISTS account (
	id BIGINT AUTO_INCREMENT PRIMARY KEY,
	user_id BIGINT NOT NULL,
	current_balance DECIMAL(19,2) DEFAULT 0,
    create_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    update_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	FOREIGN KEY (user_id) REFERENCES user (id)
         ON UPDATE RESTRICT
);

CREATE TABLE IF NOT EXISTS transaction_type (
	code VARCHAR(4) PRIMARY KEY,
	name VARCHAR(30) NOT NULL
);

INSERT INTO transaction_type 
VALUES ('BAL','Balance'),
       ('WD','Withdraw'),
	   ('DEP','Deposit'),
	   ('TXFS','Transfer')
ON DUPLICATE KEY UPDATE name = VALUES(name);

CREATE TABLE IF NOT EXISTS transaction (
	id BIGINT AUTO_INCREMENT PRIMARY KEY,
	transaction_type_code VARCHAR(4) NOT NULL,
	account_id BIGINT NOT NULL,
	amount DECIMAL(19,2) NOT NULL,
	starting_balance DECIMAL(19,2) NOT NULL,
	ending_balance DECIMAL(19,2) NOT NULL,
	destination_account_id BIGINT,
	source_account_id BIGINT,
	parent_transaction_id BIGINT,
    transaction_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	FOREIGN KEY (transaction_type_code) REFERENCES transaction_type (code) ON UPDATE RESTRICT,
	FOREIGN KEY (account_id) REFERENCES account (id) ON UPDATE RESTRICT,
	FOREIGN KEY (destination_account_id) REFERENCES account (id) ON UPDATE RESTRICT,
	FOREIGN KEY (source_account_id) REFERENCES account (id) ON UPDATE RESTRICT,
	FOREIGN KEY (parent_transaction_id) REFERENCES transaction (id) ON UPDATE RESTRICT
);