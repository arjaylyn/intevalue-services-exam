package org.arjaymillan.exam.minibank.exception;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class InvalidUserDataException extends RuntimeException {
    private static final long serialVersionUID = 31163212362750927L;

    public InvalidUserDataException(String message) {
		super(message);
    }
    
	public InvalidUserDataException(String message, Throwable cause) {
		super(message, cause);
	}
}
