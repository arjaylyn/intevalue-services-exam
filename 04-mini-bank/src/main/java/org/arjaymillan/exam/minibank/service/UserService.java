package org.arjaymillan.exam.minibank.service;

import java.math.BigDecimal;

import org.arjaymillan.exam.minibank.exception.InvalidUserDataException;
import org.arjaymillan.exam.minibank.model.Account;
import org.arjaymillan.exam.minibank.model.User;
import org.arjaymillan.exam.minibank.repo.AccountRepository;
import org.arjaymillan.exam.minibank.repo.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserService {
    
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private AccountRepository accountRepository;
    
    public void signup(User user) {
        if(user.getUsername() == null || user.getUsername().isEmpty()){
            throw new InvalidUserDataException("Username should have a value.");
        }
        if(user.getPassword() == null || user.getPassword().isEmpty()){
            throw new InvalidUserDataException("Password should have a value.");
        }

        User existingUser = userRepository.findByUsername(user.getUsername());

        if(existingUser != null){
            throw new InvalidUserDataException("Username is already existing.");
        }

        user.setPassword(encoder().encode(user.getPassword()));
        user = userRepository.save(user);

        Account account = new Account();
        account.setUser(user);
        account.setCurrentBalance(BigDecimal.ZERO);
        accountRepository.save(account);
    }

    public void resetpass(User user)
    {
        if(user.getUsername() == null || user.getUsername().isEmpty()){
            throw new InvalidUserDataException("Username should have a value.");
        }
        if(user.getPassword() == null || user.getPassword().isEmpty()){
            throw new InvalidUserDataException("Password should have a value.");
        }

        User old = userRepository.findByUsername(user.getUsername());
        
        if(old == null){
            throw new InvalidUserDataException("Username is not existing.");
        }

        old.setPassword(encoder().encode(user.getPassword()));
        userRepository.save(old);
    }

    public Iterable<User> getUsers(){
        return userRepository.findAll();
    }
    
    @Bean
    public PasswordEncoder encoder() {
        return new BCryptPasswordEncoder();
    }
}
