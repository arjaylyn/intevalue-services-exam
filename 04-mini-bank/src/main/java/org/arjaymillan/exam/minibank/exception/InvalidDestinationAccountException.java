package org.arjaymillan.exam.minibank.exception;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class InvalidDestinationAccountException extends RuntimeException  {
    private static final long serialVersionUID = 312432897462750927L;

	public InvalidDestinationAccountException(String message, Throwable cause) {
		super(message, cause);
	}
}
