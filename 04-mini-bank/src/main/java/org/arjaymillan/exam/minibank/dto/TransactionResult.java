package org.arjaymillan.exam.minibank.dto;

import java.math.BigDecimal;
import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class TransactionResult {
    
    private String username;

    private Long transactionId;

    private BigDecimal startingBalance;

    private BigDecimal endingBalance;
    
    private Date date;
    
}
