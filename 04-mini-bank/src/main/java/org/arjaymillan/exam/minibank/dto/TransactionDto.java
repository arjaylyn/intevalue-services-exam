package org.arjaymillan.exam.minibank.dto;

import java.math.BigDecimal;
import java.util.Date;

import org.arjaymillan.exam.minibank.model.Transaction;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class TransactionDto {
    private long id;
    private Date transactionDate;
    private String transactionType;
    private BigDecimal amount;
    private BigDecimal startingBalance;
    private BigDecimal endingBalance;
    private String destinationAccount;

    public TransactionDto() {}
    public TransactionDto(Transaction trans){
        this.id = trans.getId();
        this.transactionDate = trans.getTransactionDate();
        this.transactionType = trans.getTransactionType().getName();
        this.amount = trans.getAmount();
        this.startingBalance = trans.getStartingBalance();
        this.endingBalance = trans.getEndingBalance();
        if(trans.getDestinationAccount() != null) {
            this.destinationAccount = trans.getDestinationAccount().getUser().getUsername();
        }
    }
}
