package org.arjaymillan.exam.minibank.dto;

import java.math.BigDecimal;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class TransactionData {
    
    private BigDecimal amount;

    private String destinationUserName;
}
