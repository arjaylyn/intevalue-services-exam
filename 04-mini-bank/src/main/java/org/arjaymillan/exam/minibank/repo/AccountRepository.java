package org.arjaymillan.exam.minibank.repo;

import javax.persistence.LockModeType;

import org.arjaymillan.exam.minibank.model.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Query;

public interface AccountRepository extends JpaRepository<Account, Long> {

    @Lock(LockModeType.PESSIMISTIC_READ)
    @Query("SELECT a FROM Account a WHERE a.user.id = ?1")
    public Account findByUserId(Long userId);
}
