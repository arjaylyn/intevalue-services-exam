package org.arjaymillan.exam.minibank.exception;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class InsufficientFundsException extends RuntimeException  {
    private static final long serialVersionUID = 311632897462750927L;

    public InsufficientFundsException(String message) {
		super(message);
    }
    
	public InsufficientFundsException(String message, Throwable cause) {
		super(message, cause);
	}
}
