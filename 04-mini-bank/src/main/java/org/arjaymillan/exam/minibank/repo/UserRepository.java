package org.arjaymillan.exam.minibank.repo;

import org.arjaymillan.exam.minibank.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {
    public User findByUsername(String username);
}
