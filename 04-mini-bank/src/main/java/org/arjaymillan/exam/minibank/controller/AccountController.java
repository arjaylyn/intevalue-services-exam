package org.arjaymillan.exam.minibank.controller;

import java.util.List;

import org.arjaymillan.exam.minibank.dto.TransactionData;
import org.arjaymillan.exam.minibank.dto.TransactionDto;
import org.arjaymillan.exam.minibank.dto.TransactionResult;
import org.arjaymillan.exam.minibank.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping(value = "/account")
public class AccountController {
    
    @Autowired
    private AccountService accountService;

    @GetMapping(value = "/balance")
    public TransactionResult getBalance(Authentication authentication) {
        log.debug("getBalance : " + authentication.getName());
        return accountService.getBalance(authentication);
    }

    @PostMapping(value = "/deposit")
    public TransactionResult postDeposit(Authentication authentication, @RequestBody TransactionData data) {
        log.debug("postDeposit : " + authentication.getName());
        return accountService.postDeposit(authentication, data);
    }
    
    @PostMapping(value = "/withdraw")
    public TransactionResult postWithdraw(Authentication authentication, @RequestBody TransactionData data) {
        log.debug("postWithdraw : " + authentication.getName());
        return accountService.postWithdraw(authentication, data);
    }
    
    @PostMapping(value = "/transfer")
    public TransactionResult postTransfer(Authentication authentication, @RequestBody TransactionData data) {
        log.debug("postTransfer : " + authentication.getName());
        return accountService.postTransfer(authentication, data);
    }
    
    @GetMapping(value = "/transactions")
    public List<TransactionDto> getTransactions(Authentication authentication, @RequestParam Integer page) {
        log.debug("getTransactions : " + authentication.getName());
        int pageIndex = page == null ? 0 : page; 
        return accountService.getTransactions(authentication, pageIndex);
    }
}
