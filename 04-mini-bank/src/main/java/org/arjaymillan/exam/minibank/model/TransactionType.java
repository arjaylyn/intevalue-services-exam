package org.arjaymillan.exam.minibank.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import org.springframework.lang.NonNull;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Entity(name = "transaction_type")
@Getter @Setter @NoArgsConstructor @RequiredArgsConstructor
public class TransactionType {
    public final static TransactionType BAL = new TransactionType("BAL");
    public final static TransactionType WD = new TransactionType("WD");
    public final static TransactionType DEP = new TransactionType("DEP");
    public final static TransactionType TXFS = new TransactionType("TXFS");
    
    @Id
    private @NonNull String code;

    @Column(nullable = false, length = 30)
    private String name;
}
