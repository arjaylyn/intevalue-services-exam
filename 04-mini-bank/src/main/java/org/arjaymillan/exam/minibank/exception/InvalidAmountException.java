package org.arjaymillan.exam.minibank.exception;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class InvalidAmountException extends RuntimeException {
    private static final long serialVersionUID = 311632836462750927L;

    public InvalidAmountException(String message) {
		super(message);
    }
    
	public InvalidAmountException(String message, Throwable cause) {
		super(message, cause);
	}
}
