package org.arjaymillan.exam.minibank.controller;

import org.arjaymillan.exam.minibank.exception.InvalidUserDataException;
import org.arjaymillan.exam.minibank.exception.InvalidAmountException;
import org.arjaymillan.exam.minibank.exception.InsufficientFundsException;
import org.arjaymillan.exam.minibank.exception.InvalidDestinationAccountException;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {
 
	@ExceptionHandler({ InvalidDestinationAccountException.class })
	protected ResponseEntity<Object> handleNotFound(Exception ex, WebRequest request) {
		return handleExceptionInternal(ex, "Transfer account destination is not found.", 
				new HttpHeaders(), HttpStatus.NOT_FOUND, request);
	}
 
	@ExceptionHandler({InvalidAmountException.class, InsufficientFundsException.class, 
					   ConstraintViolationException.class, DataIntegrityViolationException.class,
					   InvalidUserDataException.class })
	public ResponseEntity<Object> handleBadRequest(Exception ex, WebRequest request) {
		return handleExceptionInternal(ex, ex.getLocalizedMessage(), 
				new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
	}
}