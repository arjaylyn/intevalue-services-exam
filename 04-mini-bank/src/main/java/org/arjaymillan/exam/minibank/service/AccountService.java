package org.arjaymillan.exam.minibank.service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.arjaymillan.exam.minibank.dto.TransactionData;
import org.arjaymillan.exam.minibank.dto.TransactionDto;
import org.arjaymillan.exam.minibank.dto.TransactionResult;
import org.arjaymillan.exam.minibank.exception.InsufficientFundsException;
import org.arjaymillan.exam.minibank.exception.InvalidAmountException;
import org.arjaymillan.exam.minibank.exception.InvalidDestinationAccountException;
import org.arjaymillan.exam.minibank.model.Account;
import org.arjaymillan.exam.minibank.model.Transaction;
import org.arjaymillan.exam.minibank.model.TransactionType;
import org.arjaymillan.exam.minibank.model.User;
import org.arjaymillan.exam.minibank.repo.AccountRepository;
import org.arjaymillan.exam.minibank.repo.TransactionRepository;
import org.arjaymillan.exam.minibank.repo.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

@Service
public class AccountService {
    
    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private TransactionRepository transRepository;

    @Transactional
    public TransactionResult getBalance(Authentication authentication) {
        User user = userRepository.findByUsername(authentication.getName());
        Account account = accountRepository.findByUserId(user.getId());
        Transaction transaction = new Transaction();
        transaction.setAccount(account);
        transaction.setAmount(BigDecimal.ZERO);
        transaction.setStartingBalance(account.getCurrentBalance());
        transaction.setEndingBalance(account.getCurrentBalance());
        transaction.setTransactionType(TransactionType.BAL);
        transaction.setTransactionDate(new Date());
        transaction = transRepository.save(transaction);

        return generateResult(transaction);
    }

    @Transactional
    public TransactionResult postDeposit(Authentication authentication, TransactionData data) {
        if(BigDecimal.ZERO.compareTo(data.getAmount()) >= 0){
            throw new InvalidAmountException("Amount should be more than zero.");
        }

        User user = userRepository.findByUsername(authentication.getName());
        Account account = accountRepository.findByUserId(user.getId());
        Transaction transaction = new Transaction();
        transaction.setAccount(account);
        transaction.setAmount(BigDecimal.ZERO);
        transaction.setStartingBalance(account.getCurrentBalance());
        transaction.setEndingBalance(account.getCurrentBalance().add(data.getAmount()));
        transaction.setTransactionType(TransactionType.DEP);
        transaction.setTransactionDate(new Date());
        transaction = transRepository.save(transaction);

        account.setCurrentBalance(transaction.getEndingBalance());
        account.setUpdateDate(new Date());
        account = accountRepository.save(account);

        return generateResult(transaction);
    }
    
    @Transactional
    public TransactionResult postWithdraw(Authentication authentication, TransactionData data) {
        if(BigDecimal.ZERO.compareTo(data.getAmount()) >= 0){
            throw new InvalidAmountException("Amount should be more than zero.");
        }

        User user = userRepository.findByUsername(authentication.getName());
        Account account = accountRepository.findByUserId(user.getId());

        if(account.getCurrentBalance().compareTo(data.getAmount()) < 0){
            throw new InsufficientFundsException("Insufficient amount for withdrawal.");
        }

        Transaction transaction = new Transaction();
        transaction.setAccount(account);
        transaction.setAmount(BigDecimal.ZERO);
        transaction.setStartingBalance(account.getCurrentBalance());
        transaction.setEndingBalance(account.getCurrentBalance().subtract(data.getAmount()));
        transaction.setTransactionType(TransactionType.WD);
        transaction.setTransactionDate(new Date());
        transaction = transRepository.save(transaction);

        account.setCurrentBalance(transaction.getEndingBalance());
        account.setUpdateDate(new Date());
        account = accountRepository.save(account);

        return generateResult(transaction);
    }
    
    @Transactional
    public TransactionResult postTransfer(Authentication authentication, TransactionData data) {
        if(BigDecimal.ZERO.compareTo(data.getAmount()) >= 0){
            throw new InvalidAmountException("Amount should be more than zero.");
        }

        if(data.getDestinationUserName() == null || data.getDestinationUserName().isEmpty()){
            throw new InvalidDestinationAccountException();
        }

        User user = userRepository.findByUsername(authentication.getName());
        User user2 = userRepository.findByUsername(data.getDestinationUserName());
        Account account = accountRepository.findByUserId(user.getId());
        
        if(user2 == null) {
            throw new InvalidDestinationAccountException();
        }

        if(account.getCurrentBalance().compareTo(data.getAmount()) < 0){
            throw new InsufficientFundsException("Insufficient amount for transfer.");
        }

        Account account2 = accountRepository.findByUserId(user2.getId());

        Transaction transaction = new Transaction();
        transaction.setAccount(account);
        transaction.setAmount(BigDecimal.ZERO);
        transaction.setStartingBalance(account.getCurrentBalance());
        transaction.setEndingBalance(account.getCurrentBalance().subtract(data.getAmount()));
        transaction.setTransactionType(TransactionType.TXFS);
        transaction.setTransactionDate(new Date());
        transaction.setDestinationAccount(account2);
        transaction = transRepository.save(transaction);

        account.setCurrentBalance(transaction.getEndingBalance());
        account.setUpdateDate(new Date());
        account = accountRepository.save(account);
        
        Transaction transaction2 = new Transaction();
        transaction2.setAccount(account2);
        transaction2.setAmount(BigDecimal.ZERO);
        transaction2.setStartingBalance(account2.getCurrentBalance());
        transaction2.setEndingBalance(account2.getCurrentBalance().add(data.getAmount()));
        transaction2.setTransactionType(TransactionType.TXFS);
        transaction2.setTransactionDate(new Date());
        transaction2.setSourceAccount(account);
        transaction2.setParentTransaction(transaction);
        transaction2 = transRepository.save(transaction2);

        account2.setCurrentBalance(transaction2.getEndingBalance());
        account2.setUpdateDate(new Date());
        account2 = accountRepository.save(account2);

        return generateResult(transaction);
    }

    @Transactional
    public List<TransactionDto> getTransactions(Authentication authentication, int pageIndex) {
        User user = userRepository.findByUsername(authentication.getName());
        Account account = accountRepository.findByUserId(user.getId());
        Pageable page = PageRequest.of(pageIndex, 10, Sort.by("id").descending());
        List<Transaction> trans = transRepository.findByAccountId(account.getId(), page);

        return trans.stream().map(t -> new TransactionDto(t)).collect(Collectors.toList());
    }

    private TransactionResult generateResult(Transaction trans) {

        TransactionResult result = new TransactionResult();
        result.setDate(trans.getTransactionDate());
        result.setEndingBalance(trans.getEndingBalance());
        result.setStartingBalance(trans.getStartingBalance());
        result.setUsername(trans.getAccount().getUser().getUsername());
        result.setTransactionId(trans.getId());

        return result;
    }
}
