package org.arjaymillan.exam.minibank.repo;

import java.util.List;

import org.arjaymillan.exam.minibank.model.Transaction;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface TransactionRepository extends JpaRepository<Transaction, Long> {
    
    @Query("SELECT t FROM Transaction t WHERE t.account.id = ?1")
    public List<Transaction> findByAccountId(Long accountId, Pageable pageable);
}
