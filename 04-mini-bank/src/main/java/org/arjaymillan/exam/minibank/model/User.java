package org.arjaymillan.exam.minibank.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.springframework.lang.NonNull;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter @Setter
public class User implements Serializable {
    private static final long serialVersionUID = 12849573012L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private @NonNull long id;

    @Column(length = 20, nullable = false)
    private String username;

    @Column(length = 100, nullable = false)
    private String password;

    @Column
    private Date createDate;

    @Column
    private Date updateDate;
    
}
