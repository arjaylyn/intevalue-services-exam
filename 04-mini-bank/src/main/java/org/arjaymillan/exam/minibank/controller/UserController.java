package org.arjaymillan.exam.minibank.controller;

import org.arjaymillan.exam.minibank.model.User;
import org.arjaymillan.exam.minibank.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping(value = "/users")
public class UserController {
        
    @Autowired
    private UserService userService;
    
    @PostMapping("/signup")
    public void signUp(@RequestBody User user)
    {
        userService.signup(user);
    }
    
    @PostMapping("/resetpass")
    public void resetpass(@RequestBody User user)
    {
        userService.resetpass(user);
    }

    @GetMapping
    public Iterable<User> getUsers(Authentication authentication){
        log.debug("Authentication Name : " + authentication.getName());
        return userService.getUsers();
    }

}
