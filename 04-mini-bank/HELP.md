# Read Me First
The following was discovered as part of building this project:

* The original package name 'org.arjaymillan.exam.mini-bank' is invalid and this project uses 'org.arjaymillan.exam.minibank' instead.

# Getting Started

### Configure
- Update the property file for connection details
- Schema will automaticaly be created

### Build and Run
> mvn clean install

> java -jar ./target/mini-bank-0.0.1-SNAPSHOT.jar

### User requests
* Use this to *add user* this will also create account for the user

POST http://localhost:8081/users/signup

content-type: application/json

{
    "username" : "test1",
    "password" : "test1"
}

* Use this to *reset password* of user

POST http://localhost:8081/users/resetpass

content-type: application/json

{
    "username" : "test",
    "password" : "test"
}

* Use this to *obtain Authorization Bearer*

POST http://localhost:8081/login

content-type: application/json

{
    "username" : "test",
    "password" : "test"
}

* Lists *all users*

GET http://localhost:8081/users

Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ0ZXN0IiwiZXhwIjoxNjA0MzIzNDY1fQ.WcbNOw4Mpet8q0ZRJTmKGcW_nudCfLk0tCn-HIYBJeAKRzMn5ta8bOOUNV-pXh9HgE-JQT5MbzXVDJg5tr4f1Q

### Account requests
* To know the user *balance*

GET http://localhost:8081/account/balance

Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ0ZXN0IiwiZXhwIjoxNjA0MzIzNDY1fQ.WcbNOw4Mpet8q0ZRJTmKGcW_nudCfLk0tCn-HIYBJeAKRzMn5ta8bOOUNV-pXh9HgE-JQT5MbzXVDJg5tr4f1Q

* To *deposit* amount

POST http://localhost:8081/account/deposit

Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ0ZXN0IiwiZXhwIjoxNjA0MzIzNDY1fQ.WcbNOw4Mpet8q0ZRJTmKGcW_nudCfLk0tCn-HIYBJeAKRzMn5ta8bOOUNV-pXh9HgE-JQT5MbzXVDJg5tr4f1Q

content-type: application/json

{
    "amount" : "150"
}

* Use for *withdrawing* amount

POST http://localhost:8081/account/withdraw

Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ0ZXN0IiwiZXhwIjoxNjA0MzIzNDY1fQ.WcbNOw4Mpet8q0ZRJTmKGcW_nudCfLk0tCn-HIYBJeAKRzMn5ta8bOOUNV-pXh9HgE-JQT5MbzXVDJg5tr4f1Q

content-type: application/json

{
    "amount" : "50"
}

* For *transfering* amount to another account

POST http://localhost:8081/account/transfer

Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ0ZXN0IiwiZXhwIjoxNjA0MzIzNDY1fQ.WcbNOw4Mpet8q0ZRJTmKGcW_nudCfLk0tCn-HIYBJeAKRzMn5ta8bOOUNV-pXh9HgE-JQT5MbzXVDJg5tr4f1Q

content-type: application/json

{
    "amount" : "10",
    "destinationUserName" : "test1"
}

* For getting *transactions*, page parameter should start with 0

GET http://localhost:8081/account/transactions?page=0
Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ0ZXN0IiwiZXhwIjoxNjA0MzIzNDY1fQ.WcbNOw4Mpet8q0ZRJTmKGcW_nudCfLk0tCn-HIYBJeAKRzMn5ta8bOOUNV-pXh9HgE-JQT5MbzXVDJg5tr4f1Q